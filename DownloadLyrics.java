/* DownloadLyrics.java
 * classes: Song.java
 * author: Matt Rennebohm
 * 
 * downloads lyrics from azlyrics.com
 * run as: java DownloadLyrics build [output directory] [input file]
 * 
*/
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DownloadLyrics {
	public static void main(String[] args) {
		
		if(args[0].compareTo("build") == 0)//build lyric libraries
			buildLyricsLib(args[1], args[2]);
	}	
	
	private static BufferedReader getHTML(String url) throws IOException {
	    URL host = new URL(url);
	    URLConnection con = host.openConnection();
	    BufferedReader bw = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
	    return bw;
	}
	
	private static boolean buildLyricsLib(String rootDir, String inputArtistsFile) {
				
		File logFile, outputFile_text, outputFile_html, dirA, dirR;
		BufferedReader htmlSrc_artist, htmlSrc_song, input;		
		boolean SOAFound = false, SOSFound = false;
		List<Song> songs = new ArrayList<Song>();//hold urls to lyric pages of all songs for particular artist
		String artist, curr = null, artistURL = null, songURL;				
		PrintStream log = null, output_text, output_html;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		//setup log file
		logFile = new File("log.txt");
		try {
			logFile.createNewFile();
		    log = new PrintStream(logFile);
		} catch (IOException e) {
			System.out.println("could not create log file");
			e.printStackTrace();
			return false;
		}
		
		//create root directory
		dirR = new File(rootDir);
		if (!dirR.exists())		    
		    dirR.mkdir(); 		
		
		//open input file		
		//input = new DataInputStream(new BufferedInputStream(new FileInputStream(new File(inputArtistsFile))));
		try {
			input = new BufferedReader(new FileReader(new File(inputArtistsFile)));
			//begin download
			log.println(sdf.format(new Date()) + " DOWNLOAD BEGIN");
			
			while((artist = input.readLine()) != null) {
				//fetch urls to lyric pages for all songs by artist
				artistURL = "http://www.azlyrics.com/" + artist.substring(0, 1) + "/" + artist + ".html";
				try {						
					htmlSrc_artist = getHTML(artistURL);			
				} catch (IOException e) {
					log.println(new Date() + " unable to get HTML source: " + artistURL);
					e.printStackTrace(log);	
					Thread.sleep(20000);
					continue;
				}
				try {
					while((curr = htmlSrc_artist.readLine()) != null) {
						if(curr.compareTo("var songlist = [") == 0) {
							SOAFound = true;
							continue;
						}
						if(curr.compareTo("var res = '<br />';") == 0) {
							SOAFound = false;
							break;
						}
						if(SOAFound == true) {															
							//find url and song name
							int startIndexURL, startIndexSong, endIndexURL, endIndexSong;
							for(startIndexURL = 0; curr.substring(startIndexURL, startIndexURL+3).compareTo("../") != 0; startIndexURL++);//start index of url
							for(endIndexURL = 0; curr.substring(endIndexURL, endIndexURL+5).compareTo(".html") !=0; endIndexURL++);//end index of url					
							for(startIndexSong = endIndexURL; curr.substring(startIndexSong-1, startIndexSong).compareTo("/") !=0; startIndexSong--);//start index of song					
							endIndexSong = endIndexURL;//end index of song
																									
							startIndexURL += 3;
							endIndexURL += 5;
							
							//add tupple to list
							songs.add(new Song(artist, curr.substring(startIndexSong, endIndexSong), curr.substring(startIndexURL, endIndexURL)));
						}
					}
					htmlSrc_artist.close();
				} catch (IOException e1) {
					log.println(new Date() + " unable to parse HTML source: " + curr);
					e1.printStackTrace(log);
					Thread.sleep(20000);
					continue;
				}
				//wait to avoid loosing acces to azlyrics
				Thread.sleep(20000);
			}
				
			//fetch all lyrics from resulting urls
			for(int j = 0; j < 3/*lyricPageURLS.size()*/; j++) { //uncomment to fetch all songs
				
				//create root directories
				dirA = new File(rootDir + "/" + songs.get(j).artist);
				if (!dirA.exists())		    
				    dirA.mkdir();
				
					
				//create output files
				outputFile_text = new File(rootDir + "/" + songs.get(j).artist + "/" + songs.get(j).song + ".txt");
				outputFile_html = new File(rootDir + "/" + songs.get(j).artist + "/" + songs.get(j).song + ".html");
				try {
					outputFile_text.createNewFile();					
				    output_text = new PrintStream(outputFile_text);				    
				} catch (IOException e) {
					log.println(new Date() + " could not create output file: " + rootDir + "/" + songs.get(j).artist + "/" + songs.get(j).song + ".txt");
					e.printStackTrace(log);						
					continue;
				}
				try {
					outputFile_html.createNewFile();
					output_html = new PrintStream(outputFile_html);
				} catch (IOException e) {
					log.println(new Date() + " could not create output file: " + rootDir + "/" + songs.get(j).artist + "/" + songs.get(j).song + ".html");
					e.printStackTrace(log);						
					continue;
				}
				
				songURL = "http://www.azlyrics.com/" + songs.get(j).url;
			    try {
					htmlSrc_song = getHTML(songURL);
				} catch (IOException e) {
					log.println(new Date() + " unable to get HTML source: " + songURL);
					e.printStackTrace(log);
					Thread.sleep(20000);
					continue;
				}
			    
			    //parse the source
			    try {
					while((curr = htmlSrc_song.readLine()) != null) {
						output_html.println(curr);//store raw source to backup in case parsing fails						
						if(curr.compareTo("<!-- start of lyrics -->") == 0) {
							SOSFound = true;
							continue;
						}
						if(curr.compareTo("<!-- end of lyrics -->") == 0) {
							SOSFound = false;
							break;
						}
						if(SOSFound == true)							
							output_text.println(curr.replaceFirst("<br />", ""));
					}
					htmlSrc_song.close();
				} catch (IOException e) {
					log.println(new Date() + " unable to parse HTML source: " + curr);
					e.printStackTrace(log);	
					Thread.sleep(20000);
					continue;
				}
			    
			    output_text.close();
			    output_html.close();
				//wait to avoid loosing acces to azlyrics
				Thread.sleep(20000);
			}
			
			/*for(int k = 0; k < songs.size(); k++)
				System.out.println("<" + songs.get(k).artist + ", " + songs.get(k).song + ", " + songs.get(k).url + ">");*/										
			input.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			log.println(new Date() + " could not open input file");
			e.printStackTrace(log);
		} catch (IOException e) {
			log.println(new Date() + " could not read input file");
			e.printStackTrace(log);
		} catch (InterruptedException e) {
			log.println(new Date() + " could not put thread to sleep");
			e.printStackTrace(log);
		}
		
		log.println(sdf.format(new Date()) + " DOWNLOAD END");
		log.close();
		return true;
	}
}
