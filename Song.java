
public class Song {

		public String artist;
		public String song;
		public String url;
		
		public Song(String artist, String song, String url) {
			this.artist = artist;
			this.song = song;	
			this.url = url;					
		}
}
